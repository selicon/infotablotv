module.exports = {
  devServer: {
    proxy: {
      // proxy all webpack dev-server requests starting with /api to our Spring Boot backend (localhost:8088)
      '/api': {
        target: 'http://localhost:8088',
        changeOrigin: true
      },
      '/auth': {
        target: 'http://localhost:8088',
        changeOrigin: true
      },
      '/totv': {
        target: 'http://localhost:8088',
        changeOrigin: true
      },
      '/area/default': {
        target: 'http://localhost:8088',
        changeOrigin: true
      }
    }
  },
  pages: {
    index: {
      // entry for the page
      entry: './src/main.js',
      // the source template
      template: './public/index.html',
      // output as dist/index.html
      filename: 'index.html'
    }/*,
    index2: {
      // entry for the page
      entry: './src/main2.js',
      // the source template
      template: './public/index2.html',
      // output as dist/index.html
      filename: 'index2.html'
    }*/
  }
}
