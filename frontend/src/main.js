import '@babel/polyfill'
import './plugins/vuetify'

import materialIcons from 'material-design-icons/iconfont/material-icons.css'
import Vue from 'vue'
import router from './router'
import App from './App'

Vue.use(materialIcons)

Vue.config.productionTip = false

new Vue({
  components: { App },
  router,
  render: h => h(App)
}).$mount('#app')
