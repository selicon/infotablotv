import '@babel/polyfill'
import './plugins/vuetify'

import Vue from 'vue'
import App2 from './App2'

Vue.config.productionTip = false

new Vue({
  components: { App2 },
  render: h => h(App2)
}).$mount('#tv')