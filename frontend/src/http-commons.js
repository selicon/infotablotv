import axios from 'axios'
import router from './router'

const apiOptions = {
  baseURL: `/api`
}

let AXIOS = axios.create(apiOptions)

AXIOS.interceptors.request.use(function (config) {
  const tokenType = JSON.parse(window.localStorage.getItem('authUser')).tokenType
  const accessToken = JSON.parse(window.localStorage.getItem('authUser')).accessToken
  config.headers.common['Authorization'] = tokenType + ' ' + accessToken
  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})

AXIOS.interceptors.response.use(function (responce) {
  return responce
}, function (error) {
  if (error.response.status === 401) {
    router.push('/login')
  } else {
    return Promise.reject(error)
  }
})

export default AXIOS
