import Vue from 'vue'
import Router from 'vue-router'
import AdministratorGui from './routecomponents/administratorgui/AdministratorGui'
import Registration from './routecomponents/administratorgui/auth/Registration'
import Login from './routecomponents/administratorgui/auth/Login'
import Tv from './routecomponents/tv/Tv'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'layout',
    redirect: '/administratorgui'
  },
  {
    path: '/administratorgui',
    name: 'administratorgui',
    component: AdministratorGui,
    meta: { requiresAuth: true }
  },
  {
    path: '/supersecretsignup',
    name: 'registration',
    component: Registration
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/tv',
    name: 'Tv',
    component: Tv
  }
]

const router = new Router({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const authUser = JSON.parse(window.localStorage.getItem('authUser'))
    if (authUser && authUser.accessToken) {
      next()
    } else {
      next('login')
    }
  }
  next()
})

export default router
