package edu.softwerke.monitor.model;

import java.time.LocalDateTime;

public class ContentBuilder {
    private int id;
    private ContentType type;               //type of content
    private String name;                    //name of file (or title for RSS news)
    private LocalDateTime receiptTime;      //when content record was created
    private String urlAdress;               //url adress for specific web content
    private String path;                    //where file is contained

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ContentType getType() {
        return type;
    }

    public void setType(ContentType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getReceiptTime() {
        return receiptTime;
    }

    public void setReceiptTime(LocalDateTime receiptTime) {
        this.receiptTime = receiptTime;
    }

    public String getUrlAdress() {
        return urlAdress;
    }

    public void setUrlAdress(String urlAdress) {
        this.urlAdress = urlAdress;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void copyContent(Content content) {
        setName(content.getName());
        setPath(content.getPath());
        setReceiptTime(content.getReceiptTime());
        setType(content.getType());
        setUrlAdress(content.getUrlAdress());
    }

    public Content buildContent() {
        Content content = new Content(this.type, this.receiptTime, this.name, this.urlAdress, this.path);
        copyContent(new Content(null, null, null,null, null));
        return content;
    }
}
