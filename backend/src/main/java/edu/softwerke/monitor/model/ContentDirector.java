package edu.softwerke.monitor.model;

import java.time.LocalDateTime;

public class ContentDirector {
    private static volatile ContentDirector instance;
    private ContentBuilder contentBuilder;

    private ContentDirector(ContentBuilder contentBuilder) {
        this.contentBuilder = contentBuilder;
    }

    public static ContentDirector getInstance(ContentBuilder contentBuilder) {
        if (instance == null) {
            synchronized (ContentDirector.class) {
                if (instance == null) {
                    instance = new ContentDirector(contentBuilder);
                }
            }
        }
        return instance;
    }

    public Content getAppointment() {
        contentBuilder.setId(0);
        contentBuilder.setType(ContentType.appointment);
        contentBuilder.setReceiptTime(LocalDateTime.now());
        return contentBuilder.buildContent();
    }

    public Content getEmail() {
        contentBuilder.setId(0);
        contentBuilder.setType(ContentType.email);
        contentBuilder.setReceiptTime(LocalDateTime.now());
        return contentBuilder.buildContent();
    }

    public Content getPicture() {
        contentBuilder.setId(0);
        contentBuilder.setType(ContentType.picture);
        contentBuilder.setReceiptTime(LocalDateTime.now());
        return contentBuilder.buildContent();
    }

    public Content getNews() {
        contentBuilder.setId(0);
        contentBuilder.setType(ContentType.news);
        contentBuilder.setReceiptTime(LocalDateTime.now());
        return contentBuilder.buildContent();
    }

    public Content getBreakingNews() {
        contentBuilder.setId(0);
        contentBuilder.setType(ContentType.breakingNews);
        contentBuilder.setReceiptTime(LocalDateTime.now());
        return contentBuilder.buildContent();
    }

    public Content getWebContent() {
        contentBuilder.setId(0);
        contentBuilder.setType(ContentType.webContent);
        contentBuilder.setReceiptTime(LocalDateTime.now());
        return contentBuilder.buildContent();
    }
}
