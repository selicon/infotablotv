package edu.softwerke.monitor.model;

public enum ContentType {
    appointment,
    email,
    picture,
    news,
    breakingNews,
    webContent
}
