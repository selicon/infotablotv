package edu.softwerke.monitor.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Contains information, that defines users passed registration in DB.
 * Used for authorization and authentication(requests,that need to pass auth)
 */
@Entity
public class User extends DateAudit {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private int id;

    @NotBlank
    @Size(max = 15)
    private String username; //also called login

    @NotBlank
    @Size(max = 100)
    private String password;

    public User() { }

    public User(@NotBlank @Size(max = 15) String username, @NotBlank @Size(max = 100) String password) {
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
