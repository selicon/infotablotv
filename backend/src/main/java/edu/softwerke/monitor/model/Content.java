package edu.softwerke.monitor.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * This entity is for containing information in DB about content, that used for showing on screen.
 * Contains path to file, that is in filesystem directory
 * Contains url for web content
 */
@Entity
public class Content {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private int id;
    private ContentType type;               //type of content
    private String name;                    //name of file (or title for RSS news)
    private LocalDateTime receiptTime;      //when content record was created
    private String urlAdress;               //url adress for specific web content
    private String path;                    //where file is contained

    @OneToMany(mappedBy = "content", orphanRemoval = true)
    private Set<Playlist> playlistSet;

    public Content() {
        this.receiptTime = LocalDateTime.now();
    }

    public Content(ContentType type, LocalDateTime receiptTime, String name, String urlAdress, String path) {
        this();
        this.type = type;
        this.name = name;
        this.receiptTime = receiptTime;
        this.urlAdress = urlAdress;
        this.path = path;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ContentType getType() {
        return type;
    }

    public void setType(ContentType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getReceiptTime() {
        return receiptTime;
    }

    public void setReceiptTime(LocalDateTime receiptTime) {
        this.receiptTime = receiptTime;
    }

    public String getUrlAdress() {
        return urlAdress;
    }

    public void setUrlAdress(String urlAdress) {
        this.urlAdress = urlAdress;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Set<Playlist> getPlaylistSet() {
        return playlistSet;
    }

    public void setPlaylistSet(Set<Playlist> playlistSet) {
        this.playlistSet = playlistSet;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Content)) {
            return false;
        }
        Content content = (Content) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(type, content.type);
        eb.append(name, content.name);
        eb.append(receiptTime, content.receiptTime);
        eb.append(urlAdress, content.urlAdress);
        eb.append(path, content.path);
        return eb.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(type);
        hcb.append(name);
        hcb.append(receiptTime);
        hcb.append(urlAdress);
        hcb.append(path);
        return hcb.toHashCode();
    }
}
