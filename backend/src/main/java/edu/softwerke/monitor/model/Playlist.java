package edu.softwerke.monitor.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import javax.persistence.*;

/**
 * Bundle entity between content and area.
 * Contains information of how much, how long and in what order content should be shown in area.
 */
@Entity
public class Playlist {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private int id;

    @ManyToOne
    @JoinColumn(name = "area_id")
    @JsonIgnoreProperties({"playlistSet", "screenSet"})
    private Area area;

    @ManyToOne
    @JoinColumn(name = "content_id")
    @JsonIgnoreProperties({"playlistSet"})
    private Content content;

    private int playNumber; //playlist record with less play number will be shown earlier
    private int showTime;   //how long content will be shown on screen, before going to next content

    public Playlist() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public int getPlayNumber() {
        return playNumber;
    }

    public void setPlayNumber(int playNumber) {
        this.playNumber = playNumber;
    }

    public int getShowTime() {
        return showTime;
    }

    public void setShowTime(int showTime) {
        this.showTime = showTime;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Playlist)) {
            return false;
        }
        Playlist playlist = (Playlist) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(area, playlist.area);
        eb.append(content, playlist.content);
        eb.append(playNumber, playlist.playNumber);
        eb.append(showTime, playlist.showTime);
        return eb.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(area);
        hcb.append(content);
        hcb.append(playNumber);
        hcb.append(showTime);
        return hcb.toHashCode();
    }
}
