package edu.softwerke.monitor.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import javax.persistence.*;

/**
 * Entity for managing size of area, place on the screen.
 * Area should be connected to screen to show some content, according to playlist connected to area.
 */
@Entity
public class Screen {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private int id;

    @ManyToOne
    @JoinColumn(name = "area_id")
    @JsonIgnoreProperties({"playlistSet", "screenSet"})
    private Area area;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

}
