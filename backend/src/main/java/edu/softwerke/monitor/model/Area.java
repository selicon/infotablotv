package edu.softwerke.monitor.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import javax.persistence.*;
import java.util.Set;

/**
 * entity, that responsible for showing.
 */
@Entity
public class Area {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private int id;
    private short fontSize;             //size of text on screen
    private String hexBackgroundColor;  //color of background on screen
    private boolean isActive;           //true - area is connected to screen

    @OneToMany(mappedBy = "area", orphanRemoval = true)
    private Set<Playlist> playlistSet;

    @OneToMany(mappedBy = "area")
    private Set<Screen> screenSet;

    public Area(){
    }

    public Area(short fontsize, String hexBackgroundColor){
        this.fontSize=fontsize;
        this.hexBackgroundColor =hexBackgroundColor;
        this.isActive = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public short getFontSize() {
        return fontSize;
    }

    public void setFontSize(short fontSize) {
        this.fontSize = fontSize;
    }

    public String getHexBackgroundColor() {
        return hexBackgroundColor;
    }

    public void setHexBackgroundColor(String hexBackgroundColor) {
        this.hexBackgroundColor = hexBackgroundColor;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }

    public Set<Screen> getScreenSet() {
        return screenSet;
    }

    public void setScreenSet(Set<Screen> screenSet) {
        this.screenSet = screenSet;
    }

    public Set<Playlist> getPlaylistSet() {
        return playlistSet;
    }

    public void setPlaylistSet(Set<Playlist> playlistSet) {
        this.playlistSet = playlistSet;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Area)) {
            return false;
        }
        Area area = (Area) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(fontSize, area.fontSize);
        eb.append(isActive, area.isActive);
        eb.append(isActive, area.isActive);
        return eb.isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(fontSize);
        hcb.append(isActive);
        hcb.append(hexBackgroundColor);
        return hcb.toHashCode();
    }
}
