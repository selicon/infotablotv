package edu.softwerke.monitor.config;

import edu.softwerke.monitor.service.filestorageservice.FileSystemStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.IOException;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    private final FileSystemStorageService fileSystemStorageService;
    private String path;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public WebConfig(FileSystemStorageService fileSystemStorageService) {
        this.fileSystemStorageService = fileSystemStorageService;
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/api/**").allowedMethods("GET", "POST", "PATCH", "DELETE", "OPTIONS");
        registry.addMapping("/**").allowedMethods("GET", "POST", "PATCH","DELETE", "OPTIONS");
    }
    /*doesn't work really*/
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        try {
            path = "file:///" + fileSystemStorageService.getAbsolutePicturePath() + "/";
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info(path);
        registry.addResourceHandler("/picture/**").addResourceLocations(path).setCachePeriod(0);
    }
}
