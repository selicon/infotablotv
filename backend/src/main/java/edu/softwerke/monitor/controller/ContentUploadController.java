package edu.softwerke.monitor.controller;

import edu.softwerke.monitor.payload.BreakingNewsUploadRequest;
import edu.softwerke.monitor.payload.NewsUploadRequest;
import edu.softwerke.monitor.service.filestorageservice.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller for uploading news, breaking news and files to server
 */
@RequestMapping("/api/upload")
@RestController
public class ContentUploadController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final StorageService storageService;

    @Autowired
    public ContentUploadController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping("/files/")
    public void handleFileUpload(@RequestParam("files") MultipartFile[] request) {
        for (MultipartFile file : request) {
            storageService.storeFile(file);
        }
    }

    @PostMapping("/news/")
    public void handleNewsUpload(@RequestBody NewsUploadRequest newsUploadRequest) {
        String news = newsUploadRequest.getNews();
        storageService.storeNews(news);
    }

    @PostMapping("/breakingNews/")
    public void handleBreakingNewsUpload(@RequestBody BreakingNewsUploadRequest breakingNewsUploadRequest) {
        String breakingNews = breakingNewsUploadRequest.getBreakingNews();
        storageService.storeBreakingNews(breakingNews);
    }

}