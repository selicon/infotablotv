package edu.softwerke.monitor.controller;

import edu.softwerke.monitor.model.Playlist;
import edu.softwerke.monitor.service.playlistservice.PlaylistServiceAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.*;

/**
 * Playlist resource controller
 */
@RestController
@RequestMapping("/api/playlist")
public class PlaylistController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final PlaylistServiceAdapter playlistService;

    @Autowired
    public PlaylistController(PlaylistServiceAdapter playlistService) {
        this.playlistService = playlistService;
    }

    @GetMapping("/{areaid}")
    public List<Playlist> getPlaylistsByAreaId(@PathVariable("areaid") int areaId) {
        List<Playlist> AreaPlaylists = playlistService.getPlaylists(areaId);
        AreaPlaylists.sort(Comparator.comparingInt(Playlist::getPlayNumber));
        return AreaPlaylists;
    }

    @PostMapping()
    public void addPlaylistRecord(@RequestParam(value = "contentid") int contentId,
                                  @RequestParam(value = "areaid") int areaId) {
        playlistService.addPlaylistRecord(contentId, areaId);
    }

    @DeleteMapping("/{id}")
    public void deletePlaylistRecord(@PathVariable("id") int playlistId) {
        playlistService.deleteFromPlaylist(playlistId);
    }

    @PatchMapping("/{id}/up")
    public void moveUpPlaylistRecord(@PathVariable("id") int playlistId) {
        playlistService.minusPlayNumber(playlistId);
    }

    @PatchMapping("/{id}/down")
    public void moveDownPlaylistRecord(@PathVariable("id") int playlistId) {
        playlistService.plusPlayNumber(playlistId);
    }
}
