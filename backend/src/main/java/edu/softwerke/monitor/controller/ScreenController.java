package edu.softwerke.monitor.controller;

import edu.softwerke.monitor.model.Screen;
import edu.softwerke.monitor.service.screenservice.ScreenServiceAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Screen resource controller
 */
@RestController
@RequestMapping("/api/screen")
public class ScreenController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ScreenServiceAdapter screenService;

    @Autowired
    public ScreenController(ScreenServiceAdapter screenService) {
        this.screenService = screenService;
    }

    @RequestMapping("/all")
    public List<Screen> getAllScreens() {
        return screenService.getAllScreens();
    }

    @PatchMapping("/{id}")
    public void changeScreenArea(@PathVariable("id") int screenId,
                                 @RequestParam(value = "areaid") int areaId){
        screenService.changeArea(screenId, areaId);
    }
}
