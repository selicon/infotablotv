package edu.softwerke.monitor.controller;

import edu.softwerke.monitor.model.Content;
import edu.softwerke.monitor.service.contentservice.ContentServiceAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Content resource controller
 */
@RestController
@RequestMapping("/api/content")
public class ContentController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ContentServiceAdapter contentService;

    @Autowired
    public ContentController(ContentServiceAdapter contentFileService) {
        this.contentService = contentFileService;
    }

    @GetMapping("/all")
    public List<Content> getAllContent(){
        return contentService.getAllContent();
    }

    @DeleteMapping("/{contentId}")
    public void deleteContent(@PathVariable("contentId") int contentId) {
        contentService.deleteFileRecordAndFile(contentId);
    }
}
