package edu.softwerke.monitor.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * For returning html pages.
 */
@Controller
public class PageController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping({"/administratorgui", "/login", "/supersecretsignup", "/tv"})
    public String pageDispatcher() {
        return "index.html";
    }

    /*@RequestMapping({"/tv"})
    public String pageDispatcherTv() {
        return "index2.html";
    }*/
}
