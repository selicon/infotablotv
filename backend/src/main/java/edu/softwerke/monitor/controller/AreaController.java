package edu.softwerke.monitor.controller;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.service.areaservice.AreaServiceAdapter;
import edu.softwerke.monitor.service.defaultproperties.PropertiesAggregatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Area resource controller.
 */
@RestController
public class AreaController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final AreaServiceAdapter areaService;
    private final PropertiesAggregatorService propertiesAggregatorService;

    @Autowired
    public AreaController(AreaServiceAdapter areaService,
                          PropertiesAggregatorService propertiesAggregatorService) {
        this.areaService = areaService;
        this.propertiesAggregatorService = propertiesAggregatorService;
    }

    @GetMapping("/api/area/all")
    public List<Area> getAreas() {
        return areaService.getAllAreas();
    }

    @GetMapping("area/default")
    public Area getDefaultArea() {
        return propertiesAggregatorService.getDefaultArea();
    }

    @PostMapping("/api/area/")
    public void createArea() {
        propertiesAggregatorService.addDefaultArea();
    }

    @PatchMapping("/api/area/{id}")
    public void editArea(@PathVariable("id") int areaId,
                         @RequestParam(value = "backgroundcolor") String backgroundColor,
                         @RequestParam(value = "fontsize") short fontSize) {
        areaService.changeAreaBackgroundColor(areaId, backgroundColor);
        areaService.changeAreaFontSize(areaId, fontSize);
    }

    @DeleteMapping("/api/area/{id}")
    public void deleteArea(@PathVariable("id") int areaId) {
        areaService.deleteArea(areaId);
    }
}