package edu.softwerke.monitor.controller;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Playlist;
import edu.softwerke.monitor.model.Screen;
import edu.softwerke.monitor.payload.TvDataResponse;
import edu.softwerke.monitor.service.tvservice.TvServiceAdapter;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import javax.servlet.ServletContext;
import java.util.List;

/**
 * controller for getting configuration for showing content and content itself.
 */
@RestController
public class TvController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final TvServiceAdapter tvService;

    @Autowired
    public TvController(TvServiceAdapter tvService) {
        this.tvService = tvService;
    }

    @GetMapping("/totv")
    public ResponseEntity<?> getShowConfiguration() {
        List<Screen> screens = tvService.getScreens();
        List<Area> activeAreas = tvService.getActiveAreas();
        List<List<List<Playlist>>> groupedActivePlaylists = tvService.getGroupedActivePlaylists();
        return ResponseEntity.ok(new TvDataResponse(screens, activeAreas,
                groupedActivePlaylists));
    }

    @GetMapping("/totv/content/{contentId}")
    public ResponseEntity<?> getContent(@PathVariable("contentId") int contentId) {
        return tvService.getContent(contentId);
    }
}
