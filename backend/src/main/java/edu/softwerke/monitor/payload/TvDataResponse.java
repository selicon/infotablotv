package edu.softwerke.monitor.payload;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Playlist;
import edu.softwerke.monitor.model.Screen;

import java.util.ArrayList;
import java.util.List;

/**
 * Configuration data,that returned to TV client.
 */
public class TvDataResponse {

    /*all parameters must be in the same order*/
    private List<Screen> screens = new ArrayList<>();
    private List<Area> activeAreas = new ArrayList<>();
    private List<List<List<Playlist>>> groupedActivePlaylists = new ArrayList<>();

    public TvDataResponse() {}

    public TvDataResponse(List<Screen> screens, List<Area> activeAreas,
                          List<List<List<Playlist>>> groupedActivePlaylists) {
        this.screens = screens;
        this.activeAreas = activeAreas;
        this.groupedActivePlaylists = groupedActivePlaylists;
    }

    public List<Screen> getScreens() {
        return screens;
    }

    public void setScreens(List<Screen> screens) {
        this.screens = screens;
    }

    public List<Area> getActiveAreas() {
        return activeAreas;
    }

    public void setActiveAreas(List<Area> activeAreas) {
        this.activeAreas = activeAreas;
    }

    public List<List<List<Playlist>>> getGroupedActivePlaylists() {
        return groupedActivePlaylists;
    }

    public void setGroupedActivePlaylists(List<List<List<Playlist>>> groupedActivePlaylists) {
        this.groupedActivePlaylists = groupedActivePlaylists;
    }

}
