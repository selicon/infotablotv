package edu.softwerke.monitor.payload;

/**
 * Response for registration form, to check if user with same login is already registered
 */
public class UserIdentityAvailability {
    private Boolean available; //true - user with same login not found

    public UserIdentityAvailability(Boolean available) {
        this.available = available;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }
}
