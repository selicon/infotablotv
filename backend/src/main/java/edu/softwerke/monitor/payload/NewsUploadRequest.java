package edu.softwerke.monitor.payload;

/**
 * JSON with string, that contains message is returned from content upload form.
 * This class is to identify news messages.
 */
public class NewsUploadRequest {
    private String news;

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }
}
