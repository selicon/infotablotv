package edu.softwerke.monitor.payload;

/**
 * JSON with string, that contains message is returned from content upload form.
 * This class is to identify breaking news messages.
 */
public class BreakingNewsUploadRequest {
    private String breakingNews;

    public String getBreakingNews() {
        return breakingNews;
    }

    public void setBreakingNews(String breakingNews) {
        this.breakingNews = breakingNews;
    }
}
