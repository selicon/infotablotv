package edu.softwerke.monitor.service.defaultproperties.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties ("storage")
public class StorageProperties {

    /**
     * Folder rootDirName for storing files
     */
    private String rootDirName = "content";

    public String getRootDirName() {
        return rootDirName;
    }

    public void setRootDirName(String rootDirName) {
        this.rootDirName = rootDirName;
    }

}