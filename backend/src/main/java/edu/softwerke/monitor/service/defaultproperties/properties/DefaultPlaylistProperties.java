package edu.softwerke.monitor.service.defaultproperties.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("default-content")
public class DefaultPlaylistProperties {
    private int defaultAppointmentShowTime = 10000;
    private int defaultEmailShowTime = 10000;
    private int defaultWebContentShowTime = 10000;
    private int defaultNewsShowTime = 10000;
    private int defaultBreakingNewsShowTime = 10000;
    private int defaultPictureShowTime = 10000;

    public int getDefaultAppointmentShowTime() {
        return defaultAppointmentShowTime;
    }

    public void setDefaultAppointmentShowTime(int defaultAppointmentShowTime) {
        this.defaultAppointmentShowTime = defaultAppointmentShowTime;
    }

    public int getDefaultEmailShowTime() {
        return defaultEmailShowTime;
    }

    public void setDefaultEmailShowTime(int defaultEmailShowTime) {
        this.defaultEmailShowTime = defaultEmailShowTime;
    }

    public int getDefaultWebContentShowTime() {
        return defaultWebContentShowTime;
    }

    public void setDefaultWebContentShowTime(int defaultWebContentShowTime) {
        this.defaultWebContentShowTime = defaultWebContentShowTime;
    }

    public int getDefaultNewsShowTime() {
        return defaultNewsShowTime;
    }

    public void setDefaultNewsShowTime(int defaultNewsShowTime) {
        this.defaultNewsShowTime = defaultNewsShowTime;
    }

    public int getDefaultBreakingNewsShowTime() {
        return defaultBreakingNewsShowTime;
    }

    public void setDefaultBreakingNewsShowTime(int defaultBreakingNewsShowTime) {
        this.defaultBreakingNewsShowTime = defaultBreakingNewsShowTime;
    }

    public int getDefaultPictureShowTime() {
        return defaultPictureShowTime;
    }

    public void setDefaultPictureShowTime(int defaultPictureShowTime) {
        this.defaultPictureShowTime = defaultPictureShowTime;
    }
}
