package edu.softwerke.monitor.service.tvservice;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import edu.softwerke.monitor.model.*;
import edu.softwerke.monitor.repository.AreaRepo;
import edu.softwerke.monitor.repository.PlaylistRepo;
import edu.softwerke.monitor.repository.ScreenRepo;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.apache.commons.collections4.CollectionUtils;
import javax.activation.FileTypeMap;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * TvServiceImpl provides serving configuration data for Tv client.
 * This configuration data includes order and show time of content in playlist for specific areas,
 * area configuration(background color, font size).
 * Also provides serving information from files to show content
 */
@Service
public class TvServiceImpl implements TvService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private List<Screen> screens = new ArrayList<>();
    private List<Area> activeAreas = new ArrayList<>();
    private List<List<Playlist>> activePlaylists = new ArrayList<>();
    /* playlists grouped in lists by type of content(main content,bottom line, breaking news),
     then wrapped in list(this list for some specific area), areaPlaylist then wrapped in list(to manage this lists)
     P.S. don't kill me please*/
    private List<List<List<Playlist>>> groupedActivePlaylists = new ArrayList<>();

    private final ScreenRepo screenRepo;
    private final AreaRepo areaRepo;
    private final PlaylistRepo playlistRepo;


    @Autowired
    public TvServiceImpl(ScreenRepo screenRepo, AreaRepo areaRepo, PlaylistRepo playlistRepo) {
        this.screenRepo = screenRepo;
        this.areaRepo = areaRepo;
        this.playlistRepo = playlistRepo;
    }

    @Override
    @Transactional
    @Scheduled(initialDelay = 10000, fixedRate = 10000)
    public void updateActiveAreas() {
        List<Screen> foundScreens = screenRepo.findAll();
        screens = foundScreens;

        List<Area> foundArea = areaRepo.findAreasByIsActiveIsTrue();
        for (Area area: foundArea) {
            Hibernate.initialize(area.getPlaylistSet());
            Hibernate.initialize(area.getScreenSet());
        }
        if (!(CollectionUtils.isEqualCollection(foundArea, activeAreas))) {
            activeAreas = foundArea;
        }
        List<List<Playlist>> foundPlaylists = new ArrayList<>();
        for (Area activeArea : activeAreas) {
            foundPlaylists.add(playlistRepo.findAllByArea(activeArea));
        }
        if (!(CollectionUtils.isEqualCollection(foundPlaylists, activePlaylists))) {
            activePlaylists = foundPlaylists;
            groupedActivePlaylists.clear();

            for (List<Playlist> activePlaylist : activePlaylists) {

                /*we need to group news, breaking news and other content to 3 lists*/
                List<Playlist> newsGroup = new ArrayList<>();
                List<Playlist> breakingNewsGroup = new ArrayList<>();
                List<Playlist> contentGroup = new ArrayList<>();
                for (Playlist playlist : activePlaylist) {
                    if (playlist.getContent().getType().equals(ContentType.news)) {
                        newsGroup.add(playlist);
                        continue;
                    }
                    if (playlist.getContent().getType().equals(ContentType.breakingNews)) {
                        breakingNewsGroup.add(playlist);
                        continue;
                    }
                    contentGroup.add(playlist);
                }

                /*sorting by play number, before serving*/
                newsGroup.sort(Comparator.comparingInt(Playlist::getPlayNumber));
                breakingNewsGroup.sort(Comparator.comparingInt(Playlist::getPlayNumber));
                contentGroup.sort(Comparator.comparingInt(Playlist::getPlayNumber));

                /*adding into wrapping list*/
                List<List<Playlist>> groupList = new ArrayList<>();
                groupList.add(newsGroup);
                groupList.add(breakingNewsGroup);
                groupList.add(contentGroup);

                /*adding wrapped lists into general list for management*/
                groupedActivePlaylists.add(groupList);
            }
        }
    }

    @Override
    public ResponseEntity<?> getContent(Content content) {
        if (content.getType().equals(ContentType.email) || content.getType().equals(ContentType.appointment)) {
            StringBuilder contentBuilder = new StringBuilder();
            try (Stream<String> stream = Files.lines( Paths.get(content.getPath()), StandardCharsets.UTF_8))
            {
                stream.forEach(s -> contentBuilder.append(s).append("\n"));
                return ResponseEntity.ok()
                        .body(contentBuilder.toString());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        if (content.getType().equals(ContentType.webContent)) {
            try{
                URL feedURL = new URL("https://news.yandex.ru/computers.rss");
                SyndFeedInput input = new SyndFeedInput();
                SyndFeed feed = input.build(new XmlReader(feedURL));
                List<SyndEntry> entries = feed.getEntries();
                for (SyndEntry entry : entries) {
                    if (entry.getLink().equals(content.getUrlAdress())){
                        StringBuilder contentBuilder = new StringBuilder();
                        contentBuilder.append(entry.getTitle());
                        contentBuilder.append("\n");
                        contentBuilder.append(entry.getDescription().getValue());
                        return ResponseEntity.ok()
                                .body(contentBuilder.toString());
                    }
                }
                Random rand = new Random();
                SyndEntry entry = entries.get(rand.nextInt(entries.size()));
                StringBuilder contentBuilder = new StringBuilder();
                contentBuilder.append(entry.getTitle());
                contentBuilder.append("\n");
                contentBuilder.append(entry.getDescription().getValue());
                return ResponseEntity.ok()
                        .body(contentBuilder.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (content.getType().equals(ContentType.picture)) {
            File img = new File(content.getPath());
            FileTypeMap defaultFileTypeMap = FileTypeMap.getDefaultFileTypeMap();
            String contentType = defaultFileTypeMap.getContentType(img);
            MediaType mediaType = MediaType.valueOf(contentType);
            try {
                return ResponseEntity.ok()
                        .contentType(mediaType)
                        .body(Files.readAllBytes(img.toPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public List<Screen> getScreens() {
        return screens;
    }

    @Override
    public List<Area> getActiveAreas() {
        return activeAreas;
    }

    @Override
    public List<List<List<Playlist>>> getGroupedActivePlaylists() {
        return groupedActivePlaylists;
    }
}
