package edu.softwerke.monitor.service.contentservice;

import edu.softwerke.monitor.model.Content;

import java.util.List;

public interface ContentService {
    List<Content> getAllContent();

    void deleteContentAndFile(Content content);
}
