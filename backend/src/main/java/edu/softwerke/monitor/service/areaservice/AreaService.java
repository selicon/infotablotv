package edu.softwerke.monitor.service.areaservice;

import edu.softwerke.monitor.model.Area;

import java.util.List;

public interface AreaService {

    Area getRandomArea();

    List<Area> getAllAreas();

    void changeAreaBackgroundColor(Area area, String backgroundColor);

    void changeAreaFontSize(Area area, short fontSize);

    void deleteArea(Area area);

}
