package edu.softwerke.monitor.service.tvservice;

import edu.softwerke.monitor.model.Content;
import edu.softwerke.monitor.repository.AreaRepo;
import edu.softwerke.monitor.repository.ContentRepo;
import edu.softwerke.monitor.repository.PlaylistRepo;
import edu.softwerke.monitor.repository.ScreenRepo;
import edu.softwerke.monitor.service.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.Optional;

/**
 * Class to provide resource control using id of resource
 */
@Service
public class TvServiceAdapter extends TvServiceImpl{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ContentRepo contentRepo;

    public TvServiceAdapter(ScreenRepo screenRepo, AreaRepo areaRepo, PlaylistRepo playlistRepo, ContentRepo contentRepo) {
        super(screenRepo, areaRepo, playlistRepo);
        this.contentRepo = contentRepo;
    }

    public ResponseEntity<?> getContent(int contentId) {
        Optional<Content> optionalContent = contentRepo.findById(contentId);
        Content content;
        if(optionalContent.isPresent()) {
            content = optionalContent.get();
        } else {
            throw new ResourceNotFoundException("can not return content", "Content","contentId", contentId);
        }
        return getContent(content);
    }
}
