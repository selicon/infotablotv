package edu.softwerke.monitor.service.fileservice;

public interface FileService {

    void checkNewFiles();

    void checkExistingRecords();

    void deleteFile(String filePath);
}
