package edu.softwerke.monitor.service.playlistservice;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Content;
import edu.softwerke.monitor.model.Playlist;

import java.util.List;

public interface PlaylistService {

    List<Playlist> getPlaylists(Area area);

    void addInMinActivePlaylist(Content content);

    void addPlaylistRecord(Content content, Area area);

    void deleteFromPlaylist(Playlist playlist);

    void plusPlayNumber(Playlist playlist);

    void minusPlayNumber(Playlist playlist);
}
