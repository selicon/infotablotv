package edu.softwerke.monitor.service.fileservice;

import edu.softwerke.monitor.model.Content;
import edu.softwerke.monitor.model.ContentBuilder;
import edu.softwerke.monitor.model.ContentDirector;
import edu.softwerke.monitor.model.Playlist;
import edu.softwerke.monitor.repository.ContentRepo;
import edu.softwerke.monitor.service.defaultproperties.PropertiesAggregatorService;
import edu.softwerke.monitor.service.exceptions.ServiceException;
import edu.softwerke.monitor.service.playlistservice.PlaylistService;
import org.apache.commons.io.FilenameUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import javax.activation.MimetypesFileTypeMap;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * FileServiceImpl maintains up-to-date data in DB, according to files stored in directories.
 * Checks for new files in directories and adds it to DB, when new file is found.
 * Checks existing content records in DB for existence file in directories, that content records should be connected.
 * If File doesn't exist, then deletes content record.
 * Also provides functional for deleting files.
 */
@Service
public class FileServiceImpl implements FileService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ContentRepo contentRepo;
    private final PlaylistService playlistService;
    private final String location;

    @Autowired
    public FileServiceImpl(PropertiesAggregatorService propertiesAggregatorService, ContentRepo contentRepo,
                           @Qualifier("playlistServiceImpl") PlaylistService playlistService) {
        this.contentRepo = contentRepo;
        this.location = propertiesAggregatorService.getRootDirName();
        this.playlistService = playlistService;
    }

    @Override
    @Scheduled(initialDelay = 10000,fixedRate = 300000)
    public void checkNewFiles() {
        try {
            ContentBuilder contentBuilder = new ContentBuilder();
            ContentDirector contentDirector = ContentDirector.getInstance(contentBuilder);

            File[] directories = new File(location).listFiles(File::isDirectory);
            File[] filteredDirectories = Arrays.stream(directories)
                    .filter(e -> (!e.getName().equals("other") && !e.getName().equals("webContent")))
                    .toArray(File[]::new);
            List<Content> allContent = contentRepo.findAll();

            /*checking all direcries that in root directory on 1 level*/
            for (File directory : filteredDirectories) {
                File[] files = new File(directory.getAbsolutePath()).listFiles(File::isFile); //get all files without directories
                for (File file : files) {
                    if (!containsPath(allContent, file.getAbsolutePath())) { //if found new file, than create record
                        Content content;
                        String mimetype= new MimetypesFileTypeMap().getContentType(file);
                        String type = mimetype.split("/")[0];
                        if(type.equalsIgnoreCase("image")) {
                            content = contentDirector.getPicture();
                        } else {
                            String extension = FilenameUtils.getExtension(file.getName());
                            switch (extension) {
                                case "email":
                                    content = contentDirector.getEmail();
                                break;
                                case "appointment":
                                    content = contentDirector.getAppointment();
                                break;
                                case "news":
                                    content = contentDirector.getNews();
                                break;
                                case "breakingNews":
                                    content = contentDirector.getBreakingNews();
                                break;
                                case "web":
                                    content = contentDirector.getWebContent();
                                break;
                                default: continue;
                            }
                        }
                        content.setName(file.getName());
                        content.setPath(file.getAbsolutePath());
                        contentRepo.save(content);
                        logger.info("created new content record: " +
                                "name:" + content.getName());
                    }
                }
            }
            logger.info("checked for new files");
        } catch (Exception e) {
            throw new ServiceException("Failed to add new records to DB", e);
        }
    }

    @Override
    @Scheduled(initialDelay = 11000, fixedRate = 300000)
    @Transactional
    public void checkExistingRecords() {
        try {
            /*deleting content records from db, that don't have file on server*/
            List<Content> allContent = contentRepo.findAll();
            for (Content content : allContent) {
                String path = content.getPath();
                if (!(new File(path).exists())) {
                    logger.info("deleting record without file:" +
                            " id:" + content.getId() +
                            " name: " + content.getName());
                    Hibernate.initialize(content.getPlaylistSet());
                    Set<Playlist> playlistSet = content.getPlaylistSet();
                    for (Playlist playlist : playlistSet) {
                        playlistService.deleteFromPlaylist(playlist);
                    }
                    contentRepo.delete(content);
                }
            }
            logger.info("checked existing files");
        } catch (Exception e) {
            throw new ServiceException("Failed to delete file records", e);
        }
    }

    @Override
    public void deleteFile(String filePath) {
        File file = new File(filePath);
        Path path = Paths.get(filePath);
        if (file.isDirectory()) {
            logger.info("Can not delete directories");
        }
        if (!(file.exists())) {
            logger.info("no file with path: " + filePath + " exists");
        } else {
            try {
                Files.delete(path);
            } catch (NoSuchFileException x) {
                throw new ServiceException("No such file exists", x);
            } catch (IOException x) {
                // File permission problems are caught here.
                throw new ServiceException("Problems to access to File: " + filePath, x);
            }
        }
    }

    private boolean containsPath(final List<Content> list, final String path) {
        return list.stream().anyMatch(o -> o.getPath().equals(path));
    }
}
