package edu.softwerke.monitor.service.screenservice;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Screen;

import java.util.List;

public interface ScreenService {

    List<Screen> getAllScreens();

    void changeArea(Screen screen, Area area);

}
