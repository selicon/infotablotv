package edu.softwerke.monitor.service.defaultproperties;

import edu.softwerke.monitor.model.Area;

public interface PropertiesAggregatorService {

    void createDefaultArea();

    void createScreens();

    void addDefaultArea();

    Area getDefaultArea();

    int getDefaultAreaId();

    short getDefaultAreaFontSize();

    String getDefaultAreaHexBackgroundColor();

    boolean getDefaultAreaIsActive();

    int getDefaultAppointmentShowTime();

    int getDefaultEmailShowTime();

    int getDefaultWebContentShowTime();

    int getDefaultNewsShowTime();

    int getDefaultBreakingNewsShowTime();

    int getDefaultPictureShowTime();

    String getRootDirName();

    Integer getNumberEmailsFetch();

    Integer getMaxNumberAppointmentsFetch();

    Integer getDaysScheduleFetch();

    String getEmail();

    String getEmailPassword();

    String getFeed();

    void init();

}
