package edu.softwerke.monitor.service.areaservice;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.repository.AreaRepo;
import edu.softwerke.monitor.repository.ScreenRepo;
import edu.softwerke.monitor.service.defaultproperties.PropertiesAggregatorService;
import edu.softwerke.monitor.service.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.Optional;

/**
 * Class to provide resource control using id of resource
 */
@Service
public class AreaServiceAdapter extends AreaServiceImpl {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final AreaRepo areaRepo;

    public AreaServiceAdapter(AreaRepo areaRepo, ScreenRepo screenRepo,
                              PropertiesAggregatorService propertiesAggregatorService) {
        super(areaRepo, screenRepo, propertiesAggregatorService);
        this.areaRepo = areaRepo;
    }

    public void changeAreaFontSize(int areaId, short fontSize) {
        Optional<Area> optionalArea = areaRepo.findById(areaId);
        if (optionalArea.isPresent()) {
            Area area = optionalArea.get();
            changeAreaFontSize(area, fontSize);
        } else {
            throw new ResourceNotFoundException("can not change font size.", "Area","areaId",areaId);
        }
    }

    public void deleteArea(int areaId) {
        Optional<Area> optionalArea = areaRepo.findById(areaId);
        if (optionalArea.isPresent()) {
            Area area = optionalArea.get();
            deleteArea(area);
        } else {
            throw new ResourceNotFoundException("can not delete area.", "Area","areaId",areaId);
        }
    }

    public void changeAreaBackgroundColor(int areaId, String backgroundColor) {
        Optional<Area> optionalArea = areaRepo.findById(areaId);
        if (optionalArea.isPresent()) {
            Area area = optionalArea.get();
            changeAreaBackgroundColor(area,backgroundColor);
        } else {
            throw new ResourceNotFoundException("can not change background color.", "Area","areaId",areaId);
        }
    }
}
