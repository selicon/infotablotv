package edu.softwerke.monitor.service.areaservice;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Screen;
import edu.softwerke.monitor.repository.AreaRepo;
import edu.softwerke.monitor.repository.ScreenRepo;
import edu.softwerke.monitor.service.defaultproperties.PropertiesAggregatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class for processing areas configurations and serving areas
 */
@Service
public class AreaServiceImpl implements AreaService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final AreaRepo areaRepo;
    private final ScreenRepo screenRepo;
    private final PropertiesAggregatorService propertiesAggregatorService;

    @Autowired
    public AreaServiceImpl(AreaRepo areaRepo, ScreenRepo screenRepo,
                           PropertiesAggregatorService propertiesAggregatorService) {
        this.screenRepo = screenRepo;
        this.areaRepo = areaRepo;
        this.propertiesAggregatorService = propertiesAggregatorService;
    }

    /* can be used for auto-add of new content from content udater */
    @Override
    public Area getRandomArea() {
        List<Area> areas = areaRepo.findAll();
        Collections.shuffle(areas);
        if (areas.isEmpty()) {
            return null;
        } else {
            return areas.get(0);
        }
    }

    @Override
    public List<Area> getAllAreas() {
        return areaRepo.findAll();
    }

    @Override
    public void changeAreaBackgroundColor(Area area, String backgroundColor) {
        HexColorValidator hexColorValidator = new HexColorValidator();
        if (hexColorValidator.validate(backgroundColor)){
            area.setHexBackgroundColor(backgroundColor);
            areaRepo.save(area);
        } else {
            throw new NotValidColorHexFormat("String =\"" + backgroundColor + "\" is not valid HEX format color");
        }
    }

    @Override
    public void changeAreaFontSize(Area area, short fontSize) {
        if (fontSize == 0) {
            area.setFontSize(propertiesAggregatorService.getDefaultAreaFontSize());
        } else {
            area.setFontSize(fontSize);
        }
        areaRepo.save(area);
    }

    @Override
    public void deleteArea(Area area) {
        Set<Screen> screens = area.getScreenSet();
        for (Screen screen : screens) {
            screen.setArea(null);
        }
        areaRepo.delete(area);
        screenRepo.saveAll(screens);
    }

    /* inner class for checking hex color format */
    private class HexColorValidator {

        private final Pattern pattern;
        private Matcher matcher;

        private static final String HEX_PATTERN = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$";

        HexColorValidator() {
            pattern = Pattern.compile(HEX_PATTERN);
        }

        boolean validate(final String hexColorCode) {

            matcher = pattern.matcher(hexColorCode);
            return matcher.matches();
        }
    }
}

