package edu.softwerke.monitor.service.contentupdaterservice;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import edu.softwerke.monitor.model.Content;
import edu.softwerke.monitor.model.ContentBuilder;
import edu.softwerke.monitor.model.ContentDirector;
import edu.softwerke.monitor.repository.ContentRepo;
import edu.softwerke.monitor.service.defaultproperties.PropertiesAggregatorService;
import edu.softwerke.monitor.service.exceptions.ServiceException;
import edu.softwerke.monitor.service.filestorageservice.FileSystemStorageService;
import microsoft.exchange.webservices.data.autodiscover.IAutodiscoverRedirectionUrl;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.exception.service.local.ServiceLocalException;
import microsoft.exchange.webservices.data.core.service.folder.CalendarFolder;
import microsoft.exchange.webservices.data.core.service.folder.Folder;
import microsoft.exchange.webservices.data.core.service.item.Appointment;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.schema.AppointmentSchema;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.ItemId;
import microsoft.exchange.webservices.data.search.CalendarView;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.ItemView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

/**
 * ContentUpdaterServiceImpl extracts new emails, appointments and web content to files.
 * emails and appointment files are extracted from exchange server (using EWS).
 * web content is got from specific RSS feed (using ROME)
 */
@Service
public class ContentUpdaterServiceImpl implements ContentUpdaterService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static ExchangeService service;
    private int numberEmailsFetch;
    private int maxNumberAppointmentsFetch;
    private int daysScheduleFetch;
    private String email;
    private String emailPassword;
    private String feed;
    private final FileSystemStorageService fileSystemStorageService;
    private final ContentRepo contentRepo;

    @Autowired
    public ContentUpdaterServiceImpl(FileSystemStorageService fileSystemStorageService, ContentRepo contentRepo,
                                     PropertiesAggregatorService propertiesAggregatorService) {
        this.fileSystemStorageService = fileSystemStorageService;
        this.contentRepo = contentRepo;
        this.numberEmailsFetch = propertiesAggregatorService.getNumberEmailsFetch();
        this.maxNumberAppointmentsFetch = propertiesAggregatorService.getMaxNumberAppointmentsFetch();
        this.daysScheduleFetch = propertiesAggregatorService.getDaysScheduleFetch();
        this.email = propertiesAggregatorService.getEmail();
        this.emailPassword = propertiesAggregatorService.getEmailPassword();
        this.feed = propertiesAggregatorService.getFeed();
    }

    /*adds read emails to file with name of send date and extension .email*/
    @Override
    @Scheduled(initialDelay = 8000, fixedRate = 300000)
    public void updateMail() {
        List<Map<String, String>> emailList = new ArrayList<>();

        if (service == null) {
            throw new ServiceException("ExchangeService is not initialised");
        }

        try {
            emailList = readEmails();
        } catch (ServiceException e) {
            logger.info("attempt to get emails faced problems. trying to reinitialize service");
            init();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            emailList = readEmails();
        }

        try {
            File emailDir = new File(fileSystemStorageService.getEmailPath());

            for (Map<String, String> email : emailList) {
                try {
                    String sendDate = email.get("sendDate").replaceAll("[^a-zA-Zа-яА-Я0-9\\.\\-]", "_");
                    File emailFIle = new File(emailDir.getPath() + File.separator + sendDate + ".email");
                    FileWriter writer = new FileWriter(emailFIle);
                    StringBuilder builder = new StringBuilder();
                    builder.append(email.get("subject")).append("\n\n");
                    builder.append(email.get("emailBody")).append("\n\n");
                    builder.append("Письмо от").append(email.get("senderName"));
                    writer.write(builder.toString());
                    writer.close();
                } catch (IOException e) {
                    throw new ServiceException("could not write email to file", e);
                }
            }
        } catch (IOException e) {
            throw new ServiceException("Can not resolve email directory", e);
        }
        logger.info("updated Emails");
    }

    /*single email read method*/
    private Map<String, String> readEmailItem(ItemId itemId) {
        Map<String, String> messageData = new HashMap<>();
        try {
            Item itm = Item.bind(service, itemId, PropertySet.FirstClassProperties);
            EmailMessage emailMessage = EmailMessage.bind(service, itm.getId());
            messageData.put("emailItemId", emailMessage.getId().toString());
            messageData.put("subject", emailMessage.getSubject());
            messageData.put("fromAddress", emailMessage.getFrom().getAddress());
            messageData.put("senderName", emailMessage.getSender().getName());
            Date dateTimeCreated = emailMessage.getDateTimeCreated();
            messageData.put("sendDate", dateTimeCreated.toString());
            Date dateTimeRecieved = emailMessage.getDateTimeReceived();
            messageData.put("receivedDate", dateTimeRecieved.toString());
            messageData.put("Size", emailMessage.getSize() + "");
            messageData.put("emailBody", emailMessage.getBody().toString());
        } catch (ServiceLocalException e) {
            throw new ServiceException("could not retrieve email property", e);
        } catch (Exception e) {
            throw new ServiceException("reading single email item faced problem", e);
        }
        return messageData;
    }

    /*method to fetch email items in inbox and to use singe email read method*/
    private List<Map<String, String>> readEmails() {
        List<Map<String, String>> msgDataList = new ArrayList<>();
        try {
            Folder folder = Folder.bind(service, WellKnownFolderName.Inbox);
            FindItemsResults<Item> results = service.findItems(folder.getId(), new ItemView(numberEmailsFetch));
            int i = 1;
            for (Item item : results) {
                Map<String, String> messageData;
                messageData = readEmailItem(item.getId());
                msgDataList.add(messageData);
            }
        } catch (ServiceLocalException e) {
            throw new ServiceException("could not retrieve emails by item id", e);
        } catch (Exception e) {
            throw new ServiceException("reading several emails faced problem", e);
        }
        return msgDataList;
    }

    /*adds read appointments to files*/
    @Override
    @Scheduled(initialDelay = 8000, fixedRate = 300000)
    public void updateOutlookAppointments() {
        List<Map<String, String>> appointmentsList = new ArrayList<>();

        if (service == null) {
            throw new ServiceException("ExchangeService is not initialised");
        }

        try {
            appointmentsList = readAppointments();
        } catch (ServiceException e) {
            logger.info("attempt to get appointments faced problems. trying to reinitialize service");
            init();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            appointmentsList = readAppointments();
        }

        try {
            File apntmtDir = new File(fileSystemStorageService.getAppointmentPath());
            File otherDir = new File(fileSystemStorageService.getOtherPath());

            /*adds appointment info in file with name of appointment location and .appointment extension.
            These files are created in 'other' directory */
            for (Map<String, String> appointment : appointmentsList) {
                String rawApntmtLocation = appointment.get("appointmentLocation").equals("null") ?
                        "noLocation" : appointment.get("appointmentLocation");
                String apntmtLocation = rawApntmtLocation.replaceAll("[^a-zA-Zа-яА-Я0-9\\.\\-]", "_");
                try {
                    File apntmtFile = new File(otherDir.getPath() + File.separator +
                            apntmtLocation + ".appointment");
                    FileWriter writer = new FileWriter(apntmtFile, true);
                    StringBuilder builder = new StringBuilder();
                    builder.append(appointment.get("appointmentSubject").equals("null") ?
                            "Без темы" : appointment.get("subject"));
                    builder.append(" ")
                            .append(appointment.get("appointmentStartTime"))
                            .append(" ")
                            .append(appointment.get("appointmentEndTime"))
                            .append("\n");
                    writer.append(builder.toString());
                    writer.close();
                } catch (IOException e) {
                    throw new ServiceException("could not write appointment to file", e);
                }
            }

            /*new files are replacing old .appointment files*/
            try {
                File[] files = otherDir.listFiles((d, name) -> name.endsWith(".appointment"));
                for (File file : files) {
                    Files.copy(Paths.get(file.getPath()), Paths.get(apntmtDir.getPath()).resolve(file.getName()),
                            StandardCopyOption.REPLACE_EXISTING);
                    Files.delete(Paths.get(file.getPath()));
                }
            } catch (IOException e) {
                throw new ServiceException("faced problems sending files to storeFile function", e);
            }
        } catch (IOException e) {
            throw new ServiceException("Can not resolve email directory", e);
        }
        logger.info("updated Appointments");
    }

    /*single appointment reading method*/
    private Map<String, String> readAppointment(Appointment appointment) {
        Map<String, String> appointmentData = new HashMap<>();
        try {
            appointmentData.put("appointmentItemId", appointment.getId().toString());
            appointmentData.put("appointmentSubject", appointment.getSubject());
            appointmentData.put("appointmentLocation", appointment.getLocation());
            appointmentData.put("appointmentStartTime", appointment.getStart() + "");
            appointmentData.put("appointmentEndTime", appointment.getEnd() + "");
        } catch (ServiceLocalException e) {
            throw new ServiceException("reading appointment item faced problem", e);
        }
        return appointmentData;
    }

    /*fetches Appointments and uses readAppointment to read them*/
    private List<Map<String, String>> readAppointments() {
        List<Map<String, String>> apntmtDataList = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        Date startDate = Calendar.getInstance().getTime();
        now.add(Calendar.DATE, daysScheduleFetch);
        Date endDate = now.getTime();
        try {

            /*appointments are bind to calendar, so we need to bind Exchange service to calendar*/
            CalendarFolder calendarFolder = CalendarFolder.bind(service, WellKnownFolderName.Calendar, new PropertySet());
            CalendarView cView = new CalendarView(startDate, endDate, maxNumberAppointmentsFetch);
            cView.setPropertySet(new PropertySet(AppointmentSchema.Subject, AppointmentSchema.Location,
                    AppointmentSchema.Start, AppointmentSchema.End));
            FindItemsResults<Appointment> appointments = calendarFolder.findAppointments(cView); //fetches appointments with cView config
            int i = 1;
            ArrayList<Appointment> appList = appointments.getItems();
            for (Appointment appointment : appList) {
                Map<String, String> appointmentData = readAppointment(appointment);
                apntmtDataList.add(appointmentData);
            }
        } catch (Exception e) {
            throw new ServiceException("reading several appointments faced problem", e);
        }
        return apntmtDataList;
    }

    /*connects to feed, finds new RSS news, adds content records of new web content*/
    @Override
    @Scheduled(fixedRate = 300000)
    public void updateWebContent() {
        try{
            URL feedURL = new URL(feed);
            SyndFeedInput input = new SyndFeedInput();
            SyndFeed feed = input.build(new XmlReader(feedURL));
            List<SyndEntry> entries = feed.getEntries();
            SyndEntry newEntry = entries.get(0);
            String link = newEntry.getLink();
            Optional<Content> contentByUrlAdress = contentRepo.findContentByUrlAdress(link);
            if (!contentByUrlAdress.isPresent()) {
                ContentBuilder contentBuilder = new ContentBuilder();
                ContentDirector contentDirector = ContentDirector.getInstance(contentBuilder);
                Content content = contentDirector.getWebContent();
                content.setName(newEntry.getTitle());
                content.setUrlAdress(link);
                content.setPath(fileSystemStorageService.getWebContentPath());
                contentRepo.save(content);
            }
            logger.info("updated Web content");
        } catch (Exception e) {
            throw new ServiceException("update of web content faced problems", e);
        }
    }

    /*to get access to mail service, should init ExchangeService*/
    @Override
    @PostConstruct
    public void init() {
        service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
        ExchangeCredentials credentials = new WebCredentials(email, emailPassword);
        service.setCredentials(credentials);
        try {
            service.autodiscoverUrl(email, new RedirectionUrlCallback());
        } catch (Exception e) {
            throw new ServiceException("can not initialize service", e);
        }
    }

    /*can be problems with redirctions and https. This class is created, according to EWS documentation*/
    static class RedirectionUrlCallback implements IAutodiscoverRedirectionUrl {
        public boolean autodiscoverRedirectionUrlValidationCallback(String redirectionUrl) {
            return redirectionUrl.toLowerCase().startsWith("https://");
        }
    }
}
