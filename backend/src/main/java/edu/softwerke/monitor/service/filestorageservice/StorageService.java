package edu.softwerke.monitor.service.filestorageservice;

import org.springframework.web.multipart.MultipartFile;

public interface StorageService {

    void init();

    void storeFile(MultipartFile file);

    void storeNews(String news);

    void storeBreakingNews(String breakingNews);
}
