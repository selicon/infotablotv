package edu.softwerke.monitor.service.defaultproperties;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Screen;
import edu.softwerke.monitor.repository.AreaRepo;
import edu.softwerke.monitor.repository.ScreenRepo;
import edu.softwerke.monitor.service.defaultproperties.properties.ContentUpdateProperties;
import edu.softwerke.monitor.service.defaultproperties.properties.DefaultAreaProperties;
import edu.softwerke.monitor.service.defaultproperties.properties.DefaultPlaylistProperties;
import edu.softwerke.monitor.service.defaultproperties.properties.StorageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Aggregating class for property classes.
 * Also creates 4 screens and area, if there is not enough records in DB, when starting application.
 * Creates and adds default area records.
 */
@Service
public class PropertiesAggregatorServiceImpl implements PropertiesAggregatorService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final AreaRepo areaRepo;
    private ScreenRepo screenRepo;

    private DefaultAreaProperties defaultAreaProperties;
    private DefaultPlaylistProperties defaultPlaylistProperties;
    private StorageProperties storageProperties;
    private ContentUpdateProperties contentUpdateProperties;

    /*default parameters for area*/
    private int defaultAreaId;
    private short defaultAreaFontSize;
    private String defaultAreaHexBackgroundColor;
    private boolean defaultAreaIsActive;

    /*default parameters for content*/
    private int defaultAppointmentShowTime;
    private int defaultEmailShowTime;
    private int defaultWebContentShowTime;
    private int defaultNewsShowTime;
    private int defaultBreakingNewsShowTime;
    private int defaultPictureShowTime;

    /*default directory name for storing files*/
    private String rootDirName;

    /*default parameters for content updater*/
    private int numberEmailsFetch;
    private int maxNumberAppointmentsFetch;
    private int daysScheduleFetch;
    private String email;
    private String emailPassword;
    private String feed;

    @Autowired
    public PropertiesAggregatorServiceImpl(AreaRepo areaRepo, ScreenRepo screenRepo,
                                           DefaultAreaProperties defaultAreaProperties,
                                           DefaultPlaylistProperties defaultPlaylistProperties,
                                           StorageProperties storageProperties,
                                           ContentUpdateProperties contentUpdateProperties) {
       this.areaRepo = areaRepo;
       this.screenRepo = screenRepo;
       this.defaultAreaProperties = defaultAreaProperties;
       this.storageProperties = storageProperties;
       this.defaultPlaylistProperties = defaultPlaylistProperties;
       this.contentUpdateProperties = contentUpdateProperties;
    }

    @Override
    public void createDefaultArea() {
        List<Area> areas = areaRepo.findAll();
        if (areas.isEmpty()) {
           addDefaultArea();
           logger.info("created and added default area");
        }
    }

    @Override
    public void addDefaultArea() {
        areaRepo.save(getDefaultArea());
    }

    @Override
    public Area getDefaultArea() {
        Area area = new Area();
        area.setId(defaultAreaId);
        area.setHexBackgroundColor(defaultAreaHexBackgroundColor);
        area.setFontSize(defaultAreaFontSize);
        area.setIsActive(false);
        return area;
    }

    @Override
    public void createScreens() {
        createDefaultArea();
        List<Screen> screens = screenRepo.findAll();
        if (screens.size() < 4){
            for (int i = 0; i < 4-screens.size(); i++ ){
                Screen screen = new Screen();
                screen.setArea(null);
                screenRepo.save(screen);
                logger.info("created default screen");
            }
        }
    }

    @Override
    public int getDefaultAreaId() {
        return defaultAreaId;
    }

    public void setDefaultAreaId(int defaultAreaId) {
        this.defaultAreaId = defaultAreaId;
    }

    @Override
    public short getDefaultAreaFontSize() {
        return defaultAreaFontSize;
    }

    public void setDefaultAreaFontSize(short defaultAreaFontSize) {
        this.defaultAreaFontSize = defaultAreaFontSize;
    }

    @Override
    public String getDefaultAreaHexBackgroundColor() {
        return defaultAreaHexBackgroundColor;
    }

    public void setDefaultAreaHexBackgroundColor(String defaultAreaHexBackgroundColor) {
        this.defaultAreaHexBackgroundColor = defaultAreaHexBackgroundColor;
    }

    @Override
    public boolean getDefaultAreaIsActive() {
        return defaultAreaIsActive;
    }

    public void setDefaultAreaIsActive(boolean defaultAreaIsActive) {
        this.defaultAreaIsActive = defaultAreaIsActive;
    }

    @Override
    public int getDefaultAppointmentShowTime() {
        return defaultAppointmentShowTime;
    }

    public void setDefaultAppointmentShowTime(int defaultAppointmentShowTime) {
        this.defaultAppointmentShowTime = defaultAppointmentShowTime;
    }

    @Override
    public int getDefaultEmailShowTime() {
        return defaultEmailShowTime;
    }

    public void setDefaultEmailShowTime(int defaultEmailShowTime) {
        this.defaultEmailShowTime = defaultEmailShowTime;
    }

    @Override
    public int getDefaultWebContentShowTime() {
        return defaultWebContentShowTime;
    }

    public void setDefaultWebContentShowTime(int defaultWebContentShowTime) {
        this.defaultWebContentShowTime = defaultWebContentShowTime;
    }

    @Override
    public int getDefaultNewsShowTime() {
        return defaultNewsShowTime;
    }

    public void setDefaultNewsShowTime(int defaultNewsShowTime) {
        this.defaultNewsShowTime = defaultNewsShowTime;
    }

    @Override
    public int getDefaultBreakingNewsShowTime() {
        return defaultBreakingNewsShowTime;
    }

    public void setDefaultBreakingNewsShowTime(int defaultBreakingNewsShowTime) {
        this.defaultBreakingNewsShowTime = defaultBreakingNewsShowTime;
    }

    @Override
    public int getDefaultPictureShowTime() {
        return defaultPictureShowTime;
    }

    public void setDefaultPictureShowTime(int defaultPictureShowTime) {
        this.defaultPictureShowTime = defaultPictureShowTime;
    }

    @Override
    public String getRootDirName() {
        return rootDirName;
    }

    public void setRootDirName(String rootDirName) {
        this.rootDirName = rootDirName;
    }

    @Override
    public Integer getNumberEmailsFetch() {
        return numberEmailsFetch;
    }

    public void setNumberEmailsFetch(Integer numberEmailsFetch) {
        this.numberEmailsFetch = numberEmailsFetch;
    }

    @Override
    public Integer getMaxNumberAppointmentsFetch() {
        return maxNumberAppointmentsFetch;
    }

    public void setMaxNumberAppointmentsFetch(Integer maxNumberAppointmentsFetch) {
        this.maxNumberAppointmentsFetch = maxNumberAppointmentsFetch;
    }

    @Override
    public Integer getDaysScheduleFetch() {
        return daysScheduleFetch;
    }

    public void setDaysScheduleFetch(Integer daysScheduleFetch) {
        this.daysScheduleFetch = daysScheduleFetch;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    @Override
    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }

    @Override
    @PostConstruct
    public void init() {
        setDefaultAreaId(defaultAreaProperties.getDefaultAreaId());
        setDefaultAreaFontSize(defaultAreaProperties.getDefaultAreaFontSize());
        setDefaultAreaHexBackgroundColor(defaultAreaProperties.getDefaultAreaHexBackgroundColor());
        setDefaultAreaIsActive(defaultAreaProperties.getDefaultAreaIsActive());

        setDefaultAppointmentShowTime(defaultPlaylistProperties.getDefaultAppointmentShowTime());
        setDefaultEmailShowTime(defaultPlaylistProperties.getDefaultEmailShowTime());
        setDefaultWebContentShowTime(defaultPlaylistProperties.getDefaultWebContentShowTime());
        setDefaultNewsShowTime(defaultPlaylistProperties.getDefaultNewsShowTime());
        setDefaultBreakingNewsShowTime(defaultPlaylistProperties.getDefaultBreakingNewsShowTime());
        setDefaultPictureShowTime(defaultPlaylistProperties.getDefaultPictureShowTime());

        setRootDirName(storageProperties.getRootDirName());

        setNumberEmailsFetch(contentUpdateProperties.getNumberEmailsFetch());
        setMaxNumberAppointmentsFetch(contentUpdateProperties.getMaxNumberAppointmentsFetch());
        setDaysScheduleFetch(contentUpdateProperties.getDaysScheduleFetch());
        setEmail(contentUpdateProperties.getEmail());
        setEmailPassword(contentUpdateProperties.getEmailPassword());
        setFeed(contentUpdateProperties.getFeed());

        createDefaultArea();
        createScreens();
    }
}
