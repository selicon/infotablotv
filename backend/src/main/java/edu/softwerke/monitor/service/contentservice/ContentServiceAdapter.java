package edu.softwerke.monitor.service.contentservice;

import edu.softwerke.monitor.model.Content;
import edu.softwerke.monitor.repository.ContentRepo;
import edu.softwerke.monitor.service.exceptions.ResourceNotFoundException;
import edu.softwerke.monitor.service.fileservice.FileService;
import edu.softwerke.monitor.service.playlistservice.PlaylistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.util.Optional;

/**
 * Class to provide resource control using id of resource
 */
@Service
public class ContentServiceAdapter extends ContentServiceImpl {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ContentRepo contentRepo;

    public ContentServiceAdapter(ContentRepo contentRepo,
                                 @Qualifier("playlistServiceImpl") PlaylistService playlistService,
                                 FileService contentFileService) {
        super(contentRepo, playlistService, contentFileService);
        this.contentRepo = contentRepo;
    }

    public void deleteFileRecordAndFile(int contentId) {
        Optional<Content> optionalContent = contentRepo.findById(contentId);
        Content content;
        if (optionalContent.isPresent()) {
            content = optionalContent.get();
            deleteContentAndFile(content);
        } else {
            throw new ResourceNotFoundException("can not delete content.", "Content","ContentId", contentId);
        }
    }
}
