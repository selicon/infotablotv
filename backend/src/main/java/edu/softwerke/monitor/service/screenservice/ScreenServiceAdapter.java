package edu.softwerke.monitor.service.screenservice;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Screen;
import edu.softwerke.monitor.repository.AreaRepo;
import edu.softwerke.monitor.repository.ScreenRepo;
import edu.softwerke.monitor.service.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.Optional;

/**
 * Class to provide resource control using id of resource
 */
@Service
public class ScreenServiceAdapter extends ScreenServiceImpl {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    final private ScreenRepo screenRepo;
    final private AreaRepo areaRepo;

    public ScreenServiceAdapter(ScreenRepo screenRepo, AreaRepo areaRepo) {
        super(screenRepo, areaRepo);
        this.areaRepo=areaRepo;
        this.screenRepo=screenRepo;
    }

    public void changeArea(int screenId, int newAreaId) {
        Optional<Screen> optionalScreen = screenRepo.findById(screenId);
        Optional<Area> optionalNewArea = areaRepo.findById(newAreaId);
        Screen screen;
        Area newArea;
        if (optionalScreen.isPresent()) {
            screen = optionalScreen.get();
        } else {
            throw new ResourceNotFoundException("can not change screen's area", "Screen","screenId", screenId);
        }
        if (optionalNewArea.isPresent()) {
            newArea = optionalNewArea.get();
        } else {
            throw new ResourceNotFoundException("can not change screen's area", "Area","areaId", newAreaId);
        }
        changeArea(screen, newArea);
    }
}
