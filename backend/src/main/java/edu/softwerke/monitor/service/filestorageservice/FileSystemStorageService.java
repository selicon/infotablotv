package edu.softwerke.monitor.service.filestorageservice;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import edu.softwerke.monitor.service.defaultproperties.PropertiesAggregatorService;
import edu.softwerke.monitor.service.exceptions.ServiceException;
import edu.softwerke.monitor.service.fileservice.FileService;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.PostConstruct;

/**
 * Storage class for creating directories(according to the settings in Property class) for specific content type
 * and storing uploaded files to them.
 * Also provides String path to storing directories.
 */
@Service
public class FileSystemStorageService implements StorageService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Path rootDirPath;
    private final FileService fileService;

    @Autowired
    public FileSystemStorageService(PropertiesAggregatorService propertiesAggregatorService, FileService fileService) {
        this.fileService = fileService;
        this.rootDirPath = Paths.get(propertiesAggregatorService.getRootDirName());
    }

    @Override
    public void storeFile(MultipartFile file) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new ServiceException("Failed to storeFile empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new ServiceException(
                        "Cannot storeFile file with relative path outside current directory "
                                + filename);
            }
            String mimeType = file.getContentType();
            String type = mimeType.split("/")[0];
            if (type.equalsIgnoreCase("image")) {
                Files.copy(file.getInputStream(), Paths.get(getPicturePath()).resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);
            } else {
                String extension = FilenameUtils.getExtension(file.getOriginalFilename());
                switch (extension) {
                    case "email": Files.copy(file.getInputStream(), Paths.get(getEmailPath()).resolve(filename),
                            StandardCopyOption.REPLACE_EXISTING);
                    break;
                    case "appointment": Files.copy(file.getInputStream(), Paths.get(getAppointmentPath()).resolve(filename),
                            StandardCopyOption.REPLACE_EXISTING);
                    break;
                    case "news": Files.copy(file.getInputStream(), Paths.get(getNewsPath()).resolve(filename),
                            StandardCopyOption.REPLACE_EXISTING);
                    break;
                    case "breakingNews": Files.copy(file.getInputStream(), Paths.get(getBreakingNewsPath()).resolve(filename),
                            StandardCopyOption.REPLACE_EXISTING);
                    break;
                    case "web": Files.copy(file.getInputStream(), Paths.get(getWebContentPath()).resolve(filename),
                            StandardCopyOption.REPLACE_EXISTING);
                    break;
                    default: Files.copy(file.getInputStream(), Paths.get(getOtherPath()).resolve(filename),
                            StandardCopyOption.REPLACE_EXISTING);
                }
            }
            fileService.checkNewFiles();

        } catch (IOException e) {
            throw new ServiceException("Failed to storeFile file " + filename, e);
        }
    }

    @Override
    public void storeNews(String news) {
        try {
            Stream<Path> files = Files.list(Paths.get(getNewsPath()));
            long count = files.count();
            Path path = Paths.get(getNewsPath()).resolve("news " + count + ".news");
            BufferedWriter writer = Files.newBufferedWriter(path);
            writer.write(news);
            writer.close();
            fileService.checkNewFiles();
        } catch (IOException e) {
            throw new ServiceException("Failed to store news: " + news, e);
        }
    }

    @Override
    public void storeBreakingNews(String breakingNews) {
        try {
            Stream<Path> files = Files.list(Paths.get(getNewsPath()));
            long count = files.count();
            Path path = Paths.get(getBreakingNewsPath()).resolve("breakingNews " + count + ".breakingNews");
            BufferedWriter writer = Files.newBufferedWriter(path);
            writer.write(breakingNews);
            writer.close();
            fileService.checkNewFiles();
        } catch (IOException e) {
            throw new ServiceException("Failed to store news: " + breakingNews, e);
        }
    }

    @Override
    @PostConstruct
    public void init() {
        try {
            getRootPath();
            getAppointmentPath();
            getEmailPath();
            getWebContentPath();
            getNewsPath();
            getBreakingNewsPath();
            getPicturePath();
            getOtherPath();
        } catch (IOException e) {
            throw new ServiceException("Could not initialize storage", e);
        }
    }

    public String getRootPath() throws IOException {
        Files.createDirectories(rootDirPath);
        return rootDirPath.toString();
    }

    public String getAppointmentPath() throws IOException {
        Files.createDirectories(rootDirPath.resolve("appointment"));
        return rootDirPath.resolve("appointment").toString();
    }
    public String getEmailPath() throws IOException {
        Files.createDirectories(rootDirPath.resolve("email"));
        return rootDirPath.resolve("email").toString();
    }
    public String getWebContentPath() throws IOException {
        Files.createDirectories(rootDirPath.resolve("webContent"));
        return rootDirPath.resolve("webContent").toString();
    }
    public String getNewsPath() throws IOException {
        Files.createDirectories(rootDirPath.resolve("news"));
        return rootDirPath.resolve("news").toString();
    }
    public String getBreakingNewsPath() throws IOException {
        Files.createDirectories(rootDirPath.resolve("breakingNews"));
        return rootDirPath.resolve("breakingNews").toString();
    }
    public String getPicturePath() throws IOException {
        Files.createDirectories(rootDirPath.resolve("picture"));
        return rootDirPath.resolve("picture").toString();
    }
    public String getAbsolutePicturePath() throws IOException {
        Path picturePath = Files.createDirectories(rootDirPath.resolve("picture"));
        return picturePath.toAbsolutePath().toString();
    }
    public String getOtherPath() throws IOException {
        Files.createDirectories(rootDirPath.resolve("other"));
        return rootDirPath.resolve("other").toString();
    }
}