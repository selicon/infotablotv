package edu.softwerke.monitor.service.defaultproperties.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("content-update")
public class ContentUpdateProperties {
    private int numberEmailsFetch = 30;
    private int maxNumberAppointmentsFetch = 50;
    private int daysScheduleFetch = 7;
    private String email = "temp-test-mailbox@soft-werke.com";
    private String emailPassword = "Vd6uFf4@";
    private String feed = "https://news.yandex.ru/computers.rss";

    public int getNumberEmailsFetch() {
        return numberEmailsFetch;
    }

    public void setNumberEmailsFetch(int numberEmailsFetch) {
        this.numberEmailsFetch = numberEmailsFetch;
    }

    public int getMaxNumberAppointmentsFetch() {
        return maxNumberAppointmentsFetch;
    }

    public void setMaxNumberAppointmentsFetch(int maxNumberAppointmentsFetch) {
        this.maxNumberAppointmentsFetch = maxNumberAppointmentsFetch;
    }

    public int getDaysScheduleFetch() {
        return daysScheduleFetch;
    }

    public void setDaysScheduleFetch(int daysScheduleFetch) {
        this.daysScheduleFetch = daysScheduleFetch;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }
}
