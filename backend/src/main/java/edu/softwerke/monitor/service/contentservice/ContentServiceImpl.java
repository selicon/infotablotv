package edu.softwerke.monitor.service.contentservice;

import edu.softwerke.monitor.model.Content;
import edu.softwerke.monitor.model.ContentType;
import edu.softwerke.monitor.model.Playlist;
import edu.softwerke.monitor.repository.ContentRepo;
import edu.softwerke.monitor.service.fileservice.FileService;
import edu.softwerke.monitor.service.playlistservice.PlaylistService;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

/**
 * class for serving content records and deleting them with file
 */
@Service
public class ContentServiceImpl implements ContentService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ContentRepo contentRepo;
    private final PlaylistService playlistService;
    private final FileService fileService;

    @Autowired
    public ContentServiceImpl(ContentRepo contentRepo,
                              @Qualifier("playlistServiceImpl") PlaylistService playlistService,
                              FileService fileService) {
        this.fileService = fileService;
        this.contentRepo = contentRepo;
        this.playlistService = playlistService;
    }

    @Override
    public List<Content> getAllContent() {
        return contentRepo.findAll();
    }

    @Override
    @Transactional
    public void deleteContentAndFile(Content content) {
        logger.info("Content deleted on " + content.getPath());
        if (!content.getType().equals(ContentType.webContent)){
            fileService.deleteFile(content.getPath());
        } else {
            Hibernate.initialize(content.getPlaylistSet());
            Set<Playlist> playlistSet = content.getPlaylistSet();
            for (Playlist playlist : playlistSet) {
                playlistService.deleteFromPlaylist(playlist);
            }
            contentRepo.delete(content);
        }

    }
}
