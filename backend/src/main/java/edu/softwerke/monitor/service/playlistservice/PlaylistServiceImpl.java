package edu.softwerke.monitor.service.playlistservice;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Content;
import edu.softwerke.monitor.model.Playlist;
import edu.softwerke.monitor.repository.AreaRepo;
import edu.softwerke.monitor.repository.ContentRepo;
import edu.softwerke.monitor.repository.PlaylistRepo;
import edu.softwerke.monitor.service.defaultproperties.PropertiesAggregatorService;
import edu.softwerke.monitor.service.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Set;

/**
 * PlaylistServiceImpl provides managing content show time and order for specific area
 */
@Service
public class PlaylistServiceImpl implements PlaylistService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final AreaRepo areaRepo;
    private final PlaylistRepo playlistRepo;
    private final ContentRepo contentRepo;
    private final PropertiesAggregatorService propertiesAggregatorService;

    @Autowired
    public PlaylistServiceImpl(AreaRepo areaRepo, PlaylistRepo playlistRepo, ContentRepo contentRepo,
                               PropertiesAggregatorService propertiesAggregatorService) {
        this.areaRepo = areaRepo;
        this.playlistRepo = playlistRepo;
        this.contentRepo = contentRepo;
        this.propertiesAggregatorService = propertiesAggregatorService;
    }

    @Override
    public List<Playlist> getPlaylists(Area area) {
        return playlistRepo.findAllByArea(area);
    }

    @Override
    public void addInMinActivePlaylist(Content content) {
        List<Area> activeAreas = areaRepo.findAreasByIsActiveIsTrue();

        if (activeAreas.isEmpty()){
            throw new ResourceNotFoundException("Area","isActive",null);
        } else {
            /*trying to put content in the smallest active playlist*/
            Area minArea = activeAreas.get(0);
            int minSize = minArea.getPlaylistSet().size();
            for (Area area : activeAreas) {
                if (area.getPlaylistSet().size() < minSize) {
                    minArea = area;
                    minSize = area.getPlaylistSet().size();
                }
            }
            addPlaylistRecord(content, minArea);
        }
    }

    @Override
    public void addPlaylistRecord(Content content, Area area) {
        Playlist playlist = new Playlist();
        playlist.setArea(area);
        playlist.setContent(content);
        playlist.setPlayNumber(area.getPlaylistSet().size() + 1);
        switch (content.getType()) {
            case appointment:
                playlist.setShowTime(propertiesAggregatorService.getDefaultAppointmentShowTime());
                break;
            case email:
                playlist.setShowTime(propertiesAggregatorService.getDefaultEmailShowTime());
                break;
            case webContent:
                playlist.setShowTime(propertiesAggregatorService.getDefaultWebContentShowTime());
                break;
            case news:
                playlist.setShowTime(propertiesAggregatorService.getDefaultNewsShowTime());
                break;
            case breakingNews:
                playlist.setShowTime(propertiesAggregatorService.getDefaultBreakingNewsShowTime());
                break;
            case picture:
                playlist.setShowTime(propertiesAggregatorService.getDefaultPictureShowTime());
                break;
            default:
                playlist.setShowTime(0);
                break;
        }
        playlistRepo.save(playlist);
    }

    @Override
    public void deleteFromPlaylist(Playlist playlist) {
        logger.info("" + playlist.getId());
        List<Playlist> greaterPlayNumberPlaylists = playlistRepo.
                findPlaylistsByPlayNumberGreaterThan(playlist.getPlayNumber());
        for (Playlist greaterPlayNumberPlaylist : greaterPlayNumberPlaylists) {
            greaterPlayNumberPlaylist.setPlayNumber(greaterPlayNumberPlaylist.getPlayNumber() - 1);
        }
        logger.info("" + playlist.getId());
        playlistRepo.saveAll(greaterPlayNumberPlaylists);
        Playlist playlist1 = playlistRepo.findById(playlist.getId()).get();
        playlistRepo.delete(playlist1);
    }

    @Override
    public void plusPlayNumber(Playlist playlist) {
        Set<Playlist> areaPlaylist = playlist.getArea().getPlaylistSet();
        Playlist nextPlaylist = null;
        int nextPlayNumber = playlist.getPlayNumber() + 1;
        findAndSwapPlayNumber(playlist, areaPlaylist, nextPlayNumber);
    }

    @Override
    public void minusPlayNumber(Playlist playlist) {
        Set<Playlist> areaPlaylist = playlist.getArea().getPlaylistSet();
        Playlist previousPlaylist = null;
        int previousPlayNumber = playlist.getPlayNumber() - 1;
        findAndSwapPlayNumber(playlist, areaPlaylist, previousPlayNumber);
    }

    private void findAndSwapPlayNumber(Playlist playlist, Set<Playlist> areaPlaylist, int otherPlayNumber) {
        Playlist nextPlaylist;
        for (Playlist testPlaylist : areaPlaylist) {
            if (testPlaylist.getPlayNumber() == otherPlayNumber) {
                nextPlaylist = testPlaylist;
                swapPlayNumbers(playlist, nextPlaylist);
                playlistRepo.save(playlist);
                playlistRepo.save(nextPlaylist);
                break;
            }
        }
    }

    private void swapPlayNumbers(Playlist playlist1, Playlist playlist2) {
        int playNumber1 = playlist1.getPlayNumber();
        int playNumber2 = playlist2.getPlayNumber();
        playlist1.setPlayNumber(playNumber2);
        playlist2.setPlayNumber(playNumber1);
    }
}
