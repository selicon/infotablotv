package edu.softwerke.monitor.service.defaultproperties.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("default-area")
public class DefaultAreaProperties {
    private int defaultAreaId = 0;
    private short defaultAreaFontSize = 14;
    private String defaultAreaHexBackgroundColor = "#ffffff";
    private boolean defaultAreaIsActive = false;

    public int getDefaultAreaId() {
        return defaultAreaId;
    }

    public void setDefaultAreaId(int defaultAreaId) {
        this.defaultAreaId = defaultAreaId;
    }

    public short getDefaultAreaFontSize() {
        return defaultAreaFontSize;
    }

    public void setDefaultAreaFontSize(short defaultAreaFontSize) {
        this.defaultAreaFontSize = defaultAreaFontSize;
    }

    public String getDefaultAreaHexBackgroundColor() {
        return defaultAreaHexBackgroundColor;
    }

    public void setDefaultAreaHexBackgroundColor(String defaultAreaHexBackgroundColor) {
        this.defaultAreaHexBackgroundColor = defaultAreaHexBackgroundColor;
    }

    public boolean getDefaultAreaIsActive() {
        return defaultAreaIsActive;
    }

    public void setDefaultAreaIsActive(boolean defaultAreaIsActive) {
        this.defaultAreaIsActive = defaultAreaIsActive;
    }
}
