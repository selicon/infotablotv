package edu.softwerke.monitor.service.tvservice;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Content;
import edu.softwerke.monitor.model.Playlist;
import edu.softwerke.monitor.model.Screen;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import java.util.List;

public interface TvService {
    @Scheduled(initialDelay = 10000, fixedRate = 10000)
    void updateActiveAreas();

    ResponseEntity<?> getContent(Content content);

    List<Screen> getScreens();

    List<Area> getActiveAreas();

    List<List<List<Playlist>>> getGroupedActivePlaylists();
}
