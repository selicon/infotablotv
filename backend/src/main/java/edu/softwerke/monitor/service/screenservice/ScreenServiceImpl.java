package edu.softwerke.monitor.service.screenservice;


import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Screen;
import edu.softwerke.monitor.repository.AreaRepo;
import edu.softwerke.monitor.repository.ScreenRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * ScreenServiceImpl class provides functional for processing Screen entities.
 * Responsible for serving screens and changing area connected to screen
 */
@Service
public class ScreenServiceImpl implements ScreenService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    final private ScreenRepo screenRepo;
    final private AreaRepo areaRepo;

    @Autowired
    public ScreenServiceImpl(ScreenRepo screenRepo, AreaRepo areaRepo) {
        this.screenRepo = screenRepo;
        this.areaRepo = areaRepo;
    }

    @Override
    public List<Screen> getAllScreens() {
        return screenRepo.findAll();
    }

    @Override
    public void changeArea(Screen screen, Area newArea) {
        Area oldArea = screen.getArea();
        if (oldArea != null){
            if (oldArea.getScreenSet().size() == 1) {
                oldArea.setIsActive(false);
                areaRepo.save(oldArea);
            }
        }
        screen.setArea(newArea);
        if (newArea != null) {
            newArea.setIsActive(true);
            areaRepo.save(newArea);
        }
        screenRepo.save(screen);
    }
}
