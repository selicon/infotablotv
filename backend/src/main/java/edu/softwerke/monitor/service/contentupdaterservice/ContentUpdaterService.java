package edu.softwerke.monitor.service.contentupdaterservice;

public interface ContentUpdaterService {

    void updateMail();

    void updateOutlookAppointments();

    void updateWebContent();

    void init();

}
