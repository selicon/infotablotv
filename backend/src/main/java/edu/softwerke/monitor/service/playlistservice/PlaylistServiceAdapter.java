package edu.softwerke.monitor.service.playlistservice;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Content;
import edu.softwerke.monitor.model.Playlist;
import edu.softwerke.monitor.repository.AreaRepo;
import edu.softwerke.monitor.repository.ContentRepo;
import edu.softwerke.monitor.repository.PlaylistRepo;
import edu.softwerke.monitor.service.defaultproperties.PropertiesAggregatorService;
import edu.softwerke.monitor.service.exceptions.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Class to provide resource control using id of resource
 */
@Service
public class PlaylistServiceAdapter extends PlaylistServiceImpl {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final AreaRepo areaRepo;
    private final PlaylistRepo playlistRepo;
    private final ContentRepo contentRepo;

    public PlaylistServiceAdapter(AreaRepo areaRepo, PlaylistRepo playlistRepo, ContentRepo contentRepo,
                                  PropertiesAggregatorService propertiesAggregatorService) {
        super(areaRepo, playlistRepo, contentRepo, propertiesAggregatorService);
        this.areaRepo=areaRepo;
        this.playlistRepo=playlistRepo;
        this.contentRepo=contentRepo;
    }

    public List<Playlist> getPlaylists(int areaId){
        Optional<Area> optionalArea = areaRepo.findById(areaId);
        Area area;
        if (optionalArea.isPresent()) {
            area = optionalArea.get();
            return getPlaylists(area);
        } else {
            return new ArrayList<Playlist>();
        }
    }

    public void addPlaylistRecord(int contentId, int areaId) {
        Optional<Area> optionalArea = areaRepo.findById(areaId);
        Optional<Content> optionalContent = contentRepo.findById(contentId);
        Area area;
        Content content;
        if (optionalArea.isPresent()) {
            area = optionalArea.get();
        } else {
            throw new ResourceNotFoundException("can not add to playlist.", "Area","areaId",areaId);
        }
        if (optionalContent.isPresent()) {
            content = optionalContent.get();
        } else {
            throw new ResourceNotFoundException("can not add to playlist.", "Content","ContentId", contentId);
        }
        addPlaylistRecord(content, area);
    }

    public void deleteFromPlaylist(int playlistId) {
        Optional<Playlist> optionalPlaylist = playlistRepo.findById(playlistId);
        Playlist playlist;
        if (optionalPlaylist.isPresent()) {
            playlist = optionalPlaylist.get();
        } else {
            throw new ResourceNotFoundException("can not remove playlist record", "Playlist","playlistId", playlistId);
        }
        deleteFromPlaylist(playlist);
    }

    public void plusPlayNumber(int playlistId) {
        Optional<Playlist> optionalPlaylist = playlistRepo.findById(playlistId);
        Playlist playlist;
        if (optionalPlaylist.isPresent()) {
            playlist = optionalPlaylist.get();
        } else {
            throw new ResourceNotFoundException("can not change play number(up)", "Playlist","playlistId", playlistId);
        }
        plusPlayNumber(playlist);
    }

    public void minusPlayNumber(int playlistId) {
        Optional<Playlist> optionalPlaylist = playlistRepo.findById(playlistId);
        Playlist playlist;
        if (optionalPlaylist.isPresent()) {
            playlist = optionalPlaylist.get();
        } else {
            throw new ResourceNotFoundException("can not change play number(down)", "Playlist","playlistId", playlistId);
        }
        minusPlayNumber(playlist);
    }
}
