package edu.softwerke.monitor.service.areaservice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NotValidColorHexFormat extends RuntimeException {
    public NotValidColorHexFormat(String message) {
        super(message);
    }

    public NotValidColorHexFormat(String message, Throwable cause) {
        super(message, cause);
    }
}
