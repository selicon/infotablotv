package edu.softwerke.monitor.repository;

import edu.softwerke.monitor.model.Screen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScreenRepo extends JpaRepository<Screen, Integer> {
}
