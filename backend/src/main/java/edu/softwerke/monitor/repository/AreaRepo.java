package edu.softwerke.monitor.repository;

import edu.softwerke.monitor.model.Area;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface AreaRepo extends JpaRepository<Area, Integer> {
    List<Area> findAreasByIsActiveIsTrue();
}
