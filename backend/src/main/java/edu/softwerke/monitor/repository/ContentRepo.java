package edu.softwerke.monitor.repository;

import edu.softwerke.monitor.model.Content;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface ContentRepo extends JpaRepository<Content, Integer> {
    List<Content> findAllByPath(String path);
    Optional<Content>  findContentByUrlAdress(String urlAdress);
}
