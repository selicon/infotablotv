package edu.softwerke.monitor.repository;

import edu.softwerke.monitor.model.Area;
import edu.softwerke.monitor.model.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PlaylistRepo extends JpaRepository<Playlist, Integer> {
    List<Playlist> findPlaylistsByPlayNumberGreaterThan(int playNumber);
    List<Playlist> findPlaylistByPlayNumber(int playNumber);
    List<Playlist> findPlaylistsByArea_Id(int area_id);
    List<Playlist> findAllByArea(Area area);
}
