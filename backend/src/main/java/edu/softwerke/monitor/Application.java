package edu.softwerke.monitor;

import edu.softwerke.monitor.model.ContentBuilder;
import edu.softwerke.monitor.model.ContentDirector;
import edu.softwerke.monitor.service.defaultproperties.properties.ContentUpdateProperties;
import edu.softwerke.monitor.service.defaultproperties.properties.DefaultAreaProperties;
import edu.softwerke.monitor.service.defaultproperties.properties.DefaultPlaylistProperties;
import edu.softwerke.monitor.service.defaultproperties.properties.StorageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableConfigurationProperties({StorageProperties.class, DefaultAreaProperties.class,
        DefaultPlaylistProperties.class, ContentUpdateProperties.class})
@EnableScheduling
@EnableJpaAuditing
public class Application {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner init() {
        return (args) -> ContentDirector.getInstance(new ContentBuilder());
    }
}